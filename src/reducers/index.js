import { combineReducers } from 'redux'
import getProductReducer from './main/getProductReducer'

export default combineReducers({
    getProductReducer
})
