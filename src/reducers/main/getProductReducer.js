import {
    PRODUCTS_SUCCESS,
    PRODUCTS_ERROR
} from '../../actions/const'

const initState = {
    payload: {
        data: [],
        sum: 0
    }
}

export default (state = initState, action) => {
    switch (action.type) {
        case PRODUCTS_SUCCESS:
        {
            return {
                payload: action.payload
            }
        }
        case PRODUCTS_ERROR:
            return {
                payload: action.payload
            }
        default:
            return state
    }
}
