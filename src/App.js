import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getProducts, setCurrency } from './actions/getProducts'
import List from './components/List'
import ReactSvgPieChart from "react-svg-piechart"

class App extends React.Component {
    state = {
        items: [],
        currency: 'RUB'
    }

    componentDidMount() {
        this.props.getProducts()
    }

    currencyClick = () => {
        const currency = this.state.currency === 'USD' ? 'RUB' : 'USD'
        this.props.setCurrency(this.props.product.payload, currency)
        this.setState({ currency:  currency })
    }

    render() {
        const data = []

        this.props.product.payload.data.map(i => {
            data.push({title: "Data 2", value: i.price, color: "#22594e"})
        })
        return (
            <div>
                <div onClick={this.currencyClick}><b>{this.state.currency}</b></div><br/>
                <List items={this.props.product.payload.data} />
                <hr/>
                <h2>{this.props.product.payload.sum}</h2>
                <ReactSvgPieChart data={data} />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    product: state.getProductReducer
})

const mapDispatchToProps = dispatch => ({
    getProducts: () => dispatch(getProducts()),
    setCurrency: (data, currency) => dispatch(setCurrency(data, currency))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
