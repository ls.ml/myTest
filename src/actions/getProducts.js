import axios from 'axios'
import {
    PRODUCTS_SUCCESS,
    PRODUCTS_ERROR
} from './const'

const setCurrency = (data, currency) => (dispatch) => {
    if (currency === 'USD') {
        data.data.map(i => (
            i.price = i.price*60
        ))
        data.sum = data.sum * 60
    } else {
        data.data.map(i => (
            i.price = i.price/60
        ))
        data.sum = data.sum / 60
    }
    dispatch({
        type: PRODUCTS_SUCCESS,
        payload: data
    })
}

const getProducts = () => dispatch => (
    axios.get('../data/index.json')
        .then((response) => {
            let sum = 0
            response.data.map(s => (
                  sum += s.price
            ))
            dispatch({
                type: PRODUCTS_SUCCESS,
                payload: {
                    data: response.data,
                    sum: sum 
                }
            })
        })
        .catch((error) => {
            dispatch({
                type: PRODUCTS_ERROR,
                payload: {
                    response: error
                }
            })
        })
)

export { setCurrency, getProducts }
