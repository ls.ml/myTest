import React from 'react'
import PropTypes from 'prop-types'

class Item extends React.Component {
    static propTypes = {
        children: PropTypes.object.isRequired
    }

    render() {
        return (
            <tr>
                <td>{this.props.children.name}</td>
                <td>{this.props.children.price}</td>
                <td>{this.props.children.quantity}</td>
            </tr>
        )
    }
}

export default Item
