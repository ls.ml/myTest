import React from 'react'
import PropTypes from 'prop-types'
import Item from './Item'

class List extends React.Component {
    static propTypes = {
        items: PropTypes.arrayOf(PropTypes.object).isRequired
    }

    render() {
        return (
            <div>
            <table width="50%">
                <tbody>
                    {this.props.items.map((item, index) => (
                        <Item key={`${index + 1}`}>{item}</Item>
                    ))}
                </tbody>
            </table>
            <b> </b>
            </div>
        )
    }
}

export default List
